package com.poc.gcppoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GcppocApplication {

	public static void main(String[] args) {
		SpringApplication.run(GcppocApplication.class, args);
	}

}
