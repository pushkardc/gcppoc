package com.poc.gcppoc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
    @GetMapping("/status")
    public String getApplicationStatus(){
        return "gcp poc application is up";
    }
}
